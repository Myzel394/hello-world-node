FROM node:19

WORKDIR /app

COPY . .

RUN npm install yarn
RUN yarn install --frozen-lockfile --production

EXPOSE 3000

CMD ["yarn", "start"]
